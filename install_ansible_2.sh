#!/bin/bash

# Script to install ansible 2 beta
set -ex

ANSIBLE="http://releases.ansible.com/ansible/ansible-2.0.1.0.tar.gz"

[ ! -d ".venv" ] && virtualenv .venv
.venv/bin/pip install $ANSIBLE
